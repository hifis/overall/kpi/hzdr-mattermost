### Service Usage

* HZDR Mattermost

## Plotting

* ... *Plotting is be performed in the .*

## Data + Weighing

| Data | Weighing |
| ----- | ----- |
| number of posts | 50% |
| number of teams | 25% |
| daily active users | 25% |
| monthly active users | 0% |

The activity is determined according to this page: https://docs.mattermost.com/manage/statistics.html#site-statistics  
**Daily active users** means that Mattermost was opened within the last 24 hours.  
**Monthly active users** means that Mattermost was opened within the last 30 days.  
Since the daily active users are a subset of the monthly users, there is no discernible relationship between the numbers. A daily active user can appear as a daily active user up to 30 times or only once, but still counts as a monthly user.

## Schedule

* weekly 

## Start date
17.08.2021
(previous data are not available)
